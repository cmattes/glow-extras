#pragma once

#include <vector>
#include <glow/objects/Buffer.hh>
#include <glow/objects/VertexArray.hh>
#include <glow/objects/ArrayBuffer.hh>
#include <glow/objects/ArrayBufferAttribute.hh>
#include <glow/objects/ElementArrayBuffer.hh>
#include "MeshHandle.hh"

namespace glow {
    namespace helpers {

template<typename Vertex, typename PerInstance>
class IndexedMultidraw {
public:
    using Attributes = std::vector<glow::ArrayBufferAttribute>;
    IndexedMultidraw() = default;
    IndexedMultidraw(
        const Attributes& vertexAttributes,
        const Attributes& instanceAttributes
    ) {
        setVertexAttributes(vertexAttributes);
        setInstanceAttributes(instanceAttributes);
    }

    void setInstanceAttributes(const Attributes& attrs)
    {
        mInstanceVab = glow::ArrayBuffer::create(attrs);
        mInstanceVab->setDivisor(1);
        assert(mInstanceVab->getStride() == sizeof(PerInstance));

        complete();
    }

    void setVertexAttributes(const Attributes& attrs)
    {
        mGeometryVab = glow::ArrayBuffer::create(attrs);
        assert(mGeometryVab->getStride() == sizeof(Vertex));

        complete();
    }

    MeshHandle addGeometry(const std::vector<Vertex>& vertices, std::vector<uint32_t> indices)
    {
        assert(mGeometryVab && mInstanceVab && "Attributes must be defined before adding geometry!");

        MeshHandle mesh;
        mesh.first = mIndices.size();
        mesh.count = indices.size();

        auto baseIndex = mGeometry.size();

        mGeometry.reserve(mGeometry.size() + vertices.size());
        std::copy(begin(vertices), end(vertices), back_inserter(mGeometry));
        mGeometryVab->bind().setData(mGeometry);

        // possible alternative: use baseIndex
        // => would require different type of MeshHandle
        for (auto& index : indices) index += baseIndex;
        mIndices.reserve(mIndices.size() + indices.size());
        std::copy(begin(indices), end(indices), back_inserter(mIndices));
        mIndexBuffer->bind().setIndices(mIndices);

        return mesh;
    }

    void resetInstances() {
        mPerInstanceData.clear();
        mIndirectData.clear();
    }

    void resetAll() {
        mGeometry.clear();
        resetInstances();
    }

    void add(const MeshHandle& _mesh, const PerInstance& _attributes)
    {
        if (mIndirectData.size() > 0
            && mIndirectData.back().firstIndex == _mesh.first
            && mIndirectData.back().count == _mesh.count)
        {
            // If the same mesh is supposed to be rendered multiple times in a row, just increase the instance count
            mIndirectData.back().instanceCount++;
        } else {
            mIndirectData.push_back({
                                        _mesh.count,
                                        1,
                                        _mesh.first,
                                        0,
                                        mPerInstanceData.size()
                                    });
        }
        mPerInstanceData.push_back(_attributes);
    }

    size_t instanceCount() const { return mPerInstanceData.size(); }

    void upload()
    {
        if (mPerInstanceData.size() == 0) return;
        mInstanceVab->bind().setData(mPerInstanceData);

        //{
        //    GLint previousBuffer;
        //    glGetIntegerv(GL_DRAW_INDIRECT_BUFFER_BINDING, &previousBuffer);
        //    glBindBuffer(GL_DRAW_INDIRECT_BUFFER, mIndirectBuffer->getObjectName());
        //    glBufferData(GL_DRAW_INDIRECT_BUFFER,
        //                 sizeof(IndirectBufferItem) * mIndirectData.size(),
        //                 mIndirectData.data(),
        //                 GL_STREAM_DRAW);
        //    glBindBuffer(GL_DRAW_INDIRECT_BUFFER, GLuint(previousBuffer));
        //}
    }

    void draw(GLenum mode = GL_TRIANGLES) const {
        if (mPerInstanceData.size() == 0) return;

        auto vao = mVao->bind();
        vao.negotiateBindings();
        GLint previousBuffer;
        //glGetIntegerv(GL_DRAW_INDIRECT_BUFFER_BINDING, &previousBuffer);
        //glBindBuffer(GL_DRAW_INDIRECT_BUFFER, mIndirectBuffer->getObjectName());

        glBindBuffer(GL_DRAW_INDIRECT_BUFFER, 0);
        glMultiDrawElementsIndirect(      //
            mode,                         //
            mIndexBuffer->getIndexType(), //
            mIndirectData.data(),         //
            mIndirectData.size(),         //
            0);

        //glBindBuffer(GL_DRAW_INDIRECT_BUFFER, GLuint(previousBuffer));
    }

protected:
    struct IndirectBufferItem{
        uint32_t  count;
        uint32_t  instanceCount;
        uint32_t  firstIndex;
        uint32_t  baseVertex;
        uint32_t  baseInstance;
    };

    glow::SharedVertexArray        mVao;
    glow::SharedBuffer             mIndirectBuffer;
    glow::SharedArrayBuffer        mGeometryVab;
    glow::SharedArrayBuffer        mInstanceVab;
    glow::SharedElementArrayBuffer mIndexBuffer;

    std::vector<Vertex>             mGeometry;
    std::vector<uint32_t>           mIndices;
    std::vector<PerInstance>        mPerInstanceData;
    std::vector<IndirectBufferItem> mIndirectData;

    void complete() {
        //if (!mIndirectBuffer)
        //    mIndirectBuffer = std::make_shared<glow::Buffer>(GL_DRAW_INDIRECT_BUFFER);

        if (!mIndexBuffer)
            mIndexBuffer = glow::ElementArrayBuffer::create();

        if (mInstanceVab && mGeometryVab) {
            mVao = glow::VertexArray::create(       //
                    {mGeometryVab, mInstanceVab},   //
                    mIndexBuffer,                   //
                    GL_TRIANGLES); // the mode will be ignored later :'(
        }
    }
};

    }
}
