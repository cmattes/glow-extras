#pragma once

/**
 * @brief The MeshHandle struct
 * is used by the Multidraw and IndexedMultidraw helpers,
 * allows to refer to the MeshHandle in a header file without including
 * one of the helpers
 */
struct MeshHandle
{
    unsigned int count;
    unsigned int first;
};
