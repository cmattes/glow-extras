#pragma once

namespace glow {
    namespace helpers {
        template<typename Vertex, typename PerInstance>
        class Multidraw;

        template<typename Vertex, typename PerInstance>
        class IndexedMultidraw;
    }
}
